#!/bin/bash
# Mod By WongBodo
# 
# ==================================================

# Cek root user
if [ "${EUID}" -ne 0 ]; then
echo "You need to run this script as root"
exit 1
fi

sysctl -w net.ipv6.conf.all.disable_ipv6=1 && sysctl -w net.ipv6.conf.default.disable_ipv6=1
apt update && apt install -y bzip2 gzip coreutils screen curl

red='\e[1;31m'
green='\e[0;32m'
NC='\e[0m'
MYIP=$(wget -qO- icanhazip.com);
echo "Checking VPS"
IZIN=$( curl http://ipinfo.io/ip | grep $MYIP )
if [ $MYIP = $IZIN ]; then
echo -e "${green}VPS berhasil diijinkan${NC}"
else
echo -e "${red}VPS tidak diijinkan${NC}";
echo "Please Contact Admin"
echo "Telegram   : WongBodo"
echo "WhatsApp   : xxxxxxxxxxxxxx"
exit 0
fi
mkdir /etc/v2ray;
mkdir /var/lib/premium-script;
clear
echo "Masukkan Domain Anda, Jika Anda Tidak Memiliki Domain Klik Enter"
echo "Ketikkan Perintah pointing-cf setelah proses instalasi Script Selesai"
read -p "Hostname / Domain: " host
echo "IP=$host" >> /var/lib/premium-script/ipvps.conf
echo "$host" >> /root/domain
echo "$host" >> /etc/v2ray/domain

wget https://gitlab.com/sshb1/test123/-/raw/main/services/ssh-ovpn.sh && chmod +x ssh-ovpn.sh && screen -S ssh-ovpn ./ssh-ovpn.sh
wget https://gitlab.com/sshb1/test123/-/raw/main/services/websocket-python/websocket.sh && chmod +x websocket.sh && screen -S websocket.sh ./websocket.sh
wget https://gitlab.com/sshb1/test123/-/raw/main/services/sstp.sh && chmod +x sstp.sh && screen -S sstp ./sstp.sh
wget https://gitlab.com/sshb1/test123/-/raw/main/services/wg.sh && chmod +x wg.sh && screen -S wg ./wg.sh
wget https://gitlab.com/sshb1/test123/-/raw/main/services/ssr.sh && chmod +x ssr.sh && screen -S ssr ./ssr.sh
wget https://gitlab.com/sshb1/test123/-/raw/main/services/shadowsocksobfs.sh && chmod +x shadowsocksobfs.sh && screen -S ss ./shadowsocksobfs.sh
wget https://gitlab.com/sshb1/test123/-/raw/main/services/ins-vt.sh && chmod +x ins-vt.sh && sed -i -e 's/\r$//' ins-vt.sh && screen -S v2ray ./ins-vt.sh
wget https://gitlab.com/sshb1/test123/-/raw/main/services/ipsec.sh && chmod +x ipsec.sh && screen -S ipsec ./ipsec.sh
wget https://gitlab.com/sshb1/test123/-/raw/main/services/OHP/ohpserver.sh && chmod +x ohpserver.sh && screen -S ohpserver ./ohpserver.sh
rm -f /root/ssh-ovpn.sh
rm -f /root/websocket.sh
rm -f /root/sstp.sh
rm -f /root/wg.sh
rm -f /root/ssr.sh
rm -f /root/shadowsocksobfs.sh
rm -f /root/ins-vt.sh
rm -f /root/go.sh
rm -f /root/ipsec.sh
rm -f /root/ohpserver.sh
history -c
echo "1.1" > /home/ver
clear
echo " "
echo "servicesation has been completed!!"
echo " "
echo "=================================-Script Mod By WongBodo-===========================" | tee -a log-services.txt
echo "" | tee -a log-services.txt
echo "------------------------------------------------------------------------------------" | tee -a log-services.txt
echo ""  | tee -a log-services.txt
echo "   >>> Service & Port"  | tee -a log-services.txt
echo "   - OpenSSH                  : 22, 500"  | tee -a log-services.txt
echo "   - SSH-WS CDN OpenSSH       : 2095"  | tee -a log-services.txt
echo "   - SSH-WS CDN Dropbear      : 8880"  | tee -a log-services.txt
echo "   - SSH-WS CDN SSL/TLS       : 443"  | tee -a log-services.txt
echo "   - OHP SSH                  : 8085"  | tee -a log-services.txt
echo "   - OHP Dropbear             : 8086"  | tee -a log-services.txt
echo "   - OHP OpenVPN              : 8087"  | tee -a log-services.txt
echo "   - OpenVPN-WS               : 2082"  | tee -a log-services.txt
echo "   - OpenVPN                  : TCP 1194, UDP 2200, SSL 992, X1197"  | tee -a log-services.txt
echo "   - Stunnel4 SSL/TLS         : 444"  | tee -a log-services.txt
echo "   - Dropbear                 : 143, 109"  | tee -a log-services.txt
echo "   - Squid Proxy              : 3128, 8080 (limit to IP Server)"  | tee -a log-services.txt
echo "   - Badvpn                   : 7100, 7200, 7300"  | tee -a log-services.txt
echo "   - Nginx                    : 81"  | tee -a log-services.txt
echo "   - Wireguard                : 7070"  | tee -a log-services.txt
echo "   - L2TP/IPSEC VPN           : 1701"  | tee -a log-services.txt
echo "   - PPTP VPN                 : 1732"  | tee -a log-services.txt
echo "   - SSTP VPN                 : 5555"  | tee -a log-services.txt
echo "   - Shadowsocks-R            : 1443-1543"  | tee -a log-services.txt
echo "   - SS-OBFS TLS              : 2443-2543"  | tee -a log-services.txt
echo "   - SS-OBFS HTTP             : 3443-3453"  | tee -a log-services.txt
echo "   - V2RAY Vmess TLS          : 4443"  | tee -a log-services.txt
echo "   - V2RAY Vmess None TLS     : 780"  | tee -a log-services.txt
echo "   - V2RAY Vless TLS          : 5443"  | tee -a log-services.txt
echo "   - V2RAY Vless None TLS     : 880"  | tee -a log-services.txt
echo "   - Trojan                   : 6443"  | tee -a log-services.txt
echo "   - Trojan-Go                : 7443"  | tee -a log-services.txt
echo ""  | tee -a log-services.txt
echo "   >>> Server Information & Other Features"  | tee -a log-services.txt
echo "   - Timezone                : Asia/Jakarta (GMT +7)"  | tee -a log-services.txt
echo "   - Fail2Ban                : [ON]"  | tee -a log-services.txt
echo "   - Dflate                  : [ON]"  | tee -a log-services.txt
echo "   - IPtables                : [ON]"  | tee -a log-services.txt
echo "   - Auto-Reboot             : [ON]"  | tee -a log-services.txt
echo "   - IPv6                    : [OFF]"  | tee -a log-services.txt
echo "   - Autoreboot On 00.00 GMT +7" | tee -a log-services.txt
echo "   - servicesation Log --> /root/log-services.txt"  | tee -a log-services.txt
echo ""  | tee -a log-services.txt
echo "-----------------------------------Mod by WongBodo----------------------------------" | tee -a log-services.txt
echo ""
echo " Reboot 10 Sec"
sleep 10
reboot
