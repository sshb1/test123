#!/bin/bash
# Mod By WongBodo
# 
# ==================================================

# Uninstall OHP Server
systemctl stop ohp-openssh.service
systemctl stop ohp-dropbear.service
systemctl stop ohp-ovpn.service

rm -f /etc/systemd/system/ohp-openssh.service
rm -f /etc/systemd/system/ohp-dropbear.service
rm -f /etc/systemd/system/ohp-ovpn.service
rm -f /etc/openvpn/client/client-tcp-ohp.ovpn
rm -f /home/vps/public_html/client-tcp-ohp.ovpn

# Uninstall Ipsec
service ipsec stop
service xl2tpd stop
service pptpd stop
systemctl stop xl2tpd
systemctl stop ipsec
systemctl stop pptpd

apt-get purge -y ppp xl2tpd pptpd libnss3-dev libnspr4-dev pkg-config \
  libpam0g-dev libcap-ng-dev libcap-ng-utils libselinux1-dev \
  libcurl4-nss-dev flex bison gcc make libnss3-tools libevent-dev \

rm -rf /usr/local/sbin/ipsec /usr/local/libexec/ipsec
rm -f /etc/init/ipsec.conf /lib/systemd/system/ipsec.service \
      /etc/init.d/ipsec /usr/lib/systemd/system/ipsec.service

rm -f /etc/ipsec.conf
rm -f /etc/pptpd.conf
rm -rf /etc/ppp/
rm -rf /opt/src
rm -rf /run/pluto

rm -f /etc/ipsec.conf* /etc/ipsec.secrets* /etc/ppp/chap-secrets* /etc/ppp/options.xl2tpd* \
      /etc/pam.d/pluto /etc/sysconfig/pluto /etc/default/pluto
rm -rf /etc/ipsec.d /etc/xl2tpd

#Edit /etc/iptables.rules and remove unneeded rules. Your original rules (if any) are backed up as /etc/iptables.rules.old-date-time. In addition, #edit /etc/iptables/rules.v4 if the file exists.

#Edit /etc/sysctl.conf and remove the lines after # Added by hwdsl2 VPN script.
#Edit /etc/rc.local and remove the lines after # Added by hwdsl2 VPN script. DO NOT remove exit 0 (if any).

rm -f /usr/bin/add-l2tp
rm -f /usr/bin/del-l2tp
rm -f /usr/bin/xp-l2tp
rm -f /usr/bin/add-pptp
rm -f /usr/bin/del-pptp
rm -f /usr/bin/xp-pptp
rm -f /usr/bin/renew-pptp
rm -f /usr/bin/renew-l2tp

# Uninstall V2ray & Trojan

mkdir -p /tmp/v2ray

# Uninstall Shadowshock
systemctl stop shadowsocks-libev.service

apt-get purge shadowsocks-libev -y
apt-get purge simple-obfs -y

rm -r /etc/shadowsocks-libev.*
rm -rf /etc/shadowsocks-libev/

rm -f usr/bin/add-ss
rm -f usr/bin/del-ss
rm -f usr/bin/cek-ss
rm -f usr/bin/xp-ss
rm -f usr/bin/renew-ss

# Uninstall Shadowshock-R
/etc/init.d/ssrmu stop

rm -rf /etc/init.d/ssrmu
rm -rf /usr/local/shadowsocksr
rm -rf /usr/local/lib/libsodium.so

rm -f /usr/bin/ssr
rm -f /usr/bin/add-ssr
rm -f /usr/bin/del-ssr
rm -f /usr/bin/xp-ssr
rm -f /usr/bin/renew-ssr

# Uninstall Wireguard
systemctl stop "wg-quick@wg0"

apt-get purge wireguard wireguard-tools

rm -rf /etc/wireguard/

rm -f /usr/bin/add-wg
rm -f /usr/bin/del-wg
rm -f /usr/bin/cek-wg
rm -f /usr/bin/xp-wg
rm -f /usr/bin/renew-wg

# Uninstall SSTP
systemctl stop accel-ppp

rm -f /etc/accel-ppp.conf

rm -rf/opt/accel-ppp-code/
rm -rf /home/sstp

rm -f /usr/bin/add-sstp
rm -f /usr/bin/del-sstp
rm -f /usr/bin/cek-sstp
rm -f /usr/bin/xp-sstp
rm -f /usr/bin/renew-sstp

# Uninstall Websocket
systemctl stop ws-openssh.service
systemctl stop ws-dropbear.service
systemctl stop ws-stunnel.service
systemctl stop ws-ovpn.service

rm -f ws-openssh.service
rm -f ws-dropbear.service
rm -f ws-stunnel.service
rm -f ws-ovpn.service
rm -f /usr/local/bin/ws-openssh
rm -f /usr/local/bin/ws-dropbear
rm -f /usr/local/bin/ws-stunnel
rm -f /usr/local/bin/ws-ovpn

# Uninstall SSH & Openvpn
