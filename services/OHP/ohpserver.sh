#!/bin/bash
# Mod By WongBodo
# 
# ==================================================

clear
echo Installing Over-Http-Puncher
sleep 1
echo Cek Hak Akses...
sleep 0.5
cd

#Open HTTP Puncher By EvoTeamMalaysia Project
#Direct Proxy Squid For OpenVPN TCP
#Telegram: @EvoTeamMalaysia
RED='\e[1;31m'
GREEN='\e[0;32m'
BLUE='\e[0;34m'
NC='\e[0m'
MYIP=$(wget -qO- https://icanhazip.com);
MYIP2="s/xxxxxxxxx/$MYIP/g";

#Update Repository VPS
clear
apt update 
apt-get -y upgrade

#Installing ohp Server
cd 
wget https://github.com/lfasmpao/open-http-puncher/releases/download/0.1/ohpserver-linux32.zip
unzip ohpserver-linux32.zip
rm -f ohpserver-linux32.zip
chmod +x ohpserver
mv /root/ohpserver /usr/local/bin/ohpserver

#Buat File OpenVPN TCP OHP
cat > /etc/openvpn/client-tcp-ohp.ovpn <<END
############# WELCOME TO #############
########## MOD BY WONGBODO ###########
######## WONGBODO OHP SERVER ########
setenv CLIENT_CERT 0
setenv opt block-outside-dns
client
dev tun
proto tcp
remote "bug" 1194
persist-tun
persist-key
persist-remote-ip
comp-lzo
reneg-sec 0
pull
resolv-retry infinite
remote-cert-tls server
verb 3
auth-user-pass
cipher none
auth none
auth-nocache
script-security 2
tls-version-min 1.2
tls-cipher TLS-ECDHE-ECDSA-WITH-AES-128-GCM-SHA256

http-proxy xxxxxxxxx 8087
http-proxy-option VERSION 1.1
http-proxy-option CUSTOM-HEADER ""
http-proxy-option CUSTOM-HEADER "Host: "
http-proxy-option CUSTOM-HEADER "X-Forwarded-Host: "
http-proxy-option CUSTOM-HEADER ""
END

sed -i $MYIP2 /etc/openvpn/client-tcp-ohp.ovpn;

# masukkan certificatenya ke dalam config client TCP 1194
echo '<ca>' >> /etc/openvpn/client-tcp-ohp.ovpn
cat /etc/openvpn/ca.crt >> /etc/openvpn/client-tcp-ohp.ovpn
echo '</ca>' >> /etc/openvpn/client-tcp-ohp.ovpn
cp /etc/openvpn/client-tcp-ohp.ovpn /home/vps/public_html/client-tcp-ohp.ovpn
clear
cd 

#System OHP
cd /etc/systemd/system/
#System OHP-OpenSSH
wget -O /etc/systemd/system/ohp-openssh.service https://gitlab.com/sshb1/test123/-/raw/main/services/OHP/ohp-openssh.service
#System OHP-Dropbear
wget -O /etc/systemd/system/ohp-dropbear.service https://gitlab.com/sshb1/test123/-/raw/main/services/OHP/ohp-dropbear.service
#System OHP-OpenVPN
wget -O /etc/systemd/system/ohp-ovpn.service https://gitlab.com/sshb1/test123/-/raw/main/services/OHP/ohp-ovpn.service

#
cd
systemctl daemon-reload

systemctl enable ohp-openssh.service
systemctl start ohp-openssh.service
systemctl restart ohp-openssh.service

systemctl enable ohp-dropbear.service
systemctl start ohp-dropbear.service
systemctl restart ohp-dropbear.service

systemctl enable ohp-ovpn.service
systemctl start ohp-ovpn.service
systemctl restart ohp-ovpn.service

echo ""
echo -e "${GREEN}Done Installing OHP Server${NC}"
rm -f /root/ohp.sh
