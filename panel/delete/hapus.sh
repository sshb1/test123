#!/bin/bash
# Mod By WongBodo
# 
# ==================================================

#Script untuk menghapus user SSH & OpenVPN

read -p "Nama user SSH yang akan dihapus : " Pengguna

if getent passwd $Pengguna > /dev/null 2>&1; then
        userdel $Pengguna
        echo -e "User $Pengguna telah dihapus."
        echo -e "==============================="
        echo -e "Mod by WongBodo"
        echo -e "==============================="
else
        echo -e "GAGAL: User $Pengguna tidak ada."
        echo -e "==============================="
        echo -e "Mod by WongBodo"
        echo -e "==============================="
fi
