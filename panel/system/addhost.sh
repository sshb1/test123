#!/bin/bash
# By WongBodo
# 
# ==================================================

red='\e[1;31m'
green='\e[0;32m'
NC='\e[0m'
MYIP=$(wget -qO- icanhazip.com);
apt install jq curl -y
rm -f /root/domain
rm -f /etc/v2ray/domain
rm -f /var/lib/premium-script/ipvps.conf
rm -f /var/lib/premium-script/subdomain.conf
rm -f /var/lib/premium-script/cf_id.conf
rm -f /var/lib/premium-script/cf_key.conf

clear
read -rp "Masukkan Domain: " -e DOMAIN
echo ""
echo "Domain: ${DOMAIN}"
echo ""
read -rp "Masukkan Subdomain: " -e sub
echo ""
echo "Sub Domain: ${sub}"
echo ""
read -rp "Masukkan email cloudflare: " -e email
echo ""
echo "email Cloudflare: $email"
echo ""
read -rp "Masukkan token api key cloudflare: " -e key
echo ""
echo "api key Cloudfrlare: $key"
echo ""
SUB_DOMAIN=${sub}.${DOMAIN}
CF_ID=$email
CF_KEY=$key
set -euo pipefail
IP=$(wget -qO- ipinfo.io/ip);
echo "Pointing DNS Untuk Domain ${SUB_DOMAIN}..."
ZONE=$(curl -sLX GET "https://api.cloudflare.com/client/v4/zones?name=${DOMAIN}&status=active" \
     -H "X-Auth-Email: ${CF_ID}" \
     -H "X-Auth-Key: ${CF_KEY}" \
     -H "Content-Type: application/json" | jq -r .result[0].id)

RECORD=$(curl -sLX GET "https://api.cloudflare.com/client/v4/zones/${ZONE}/dns_records?name=${SUB_DOMAIN}" \
     -H "X-Auth-Email: ${CF_ID}" \
     -H "X-Auth-Key: ${CF_KEY}" \
     -H "Content-Type: application/json" | jq -r .result[0].id)

if [[ "${#RECORD}" -le 10 ]]; then
     RECORD=$(curl -sLX POST "https://api.cloudflare.com/client/v4/zones/${ZONE}/dns_records" \
     -H "X-Auth-Email: ${CF_ID}" \
     -H "X-Auth-Key: ${CF_KEY}" \
     -H "Content-Type: application/json" \
     --data '{"type":"A","name":"'${SUB_DOMAIN}'","content":"'${IP}'","ttl":120,"proxied":false}' | jq -r .result.id)
fi

RESULT=$(curl -sLX PUT "https://api.cloudflare.com/client/v4/zones/${ZONE}/dns_records/${RECORD}" \
     -H "X-Auth-Email: ${CF_ID}" \
     -H "X-Auth-Key: ${CF_KEY}" \
     -H "Content-Type: application/json" \
     --data '{"type":"A","name":"'${SUB_DOMAIN}'","content":"'${IP}'","ttl":120,"proxied":false}')
echo "Host : $SUB_DOMAIN"
echo $SUB_DOMAIN > /root/domain
echo $SUB_DOMAIN > /etc/v2ray/domain
echo "IP=$SUB_DOMAIN" >> /var/lib/premium-script/ipvps.conf
echo ${sub} >> /var/lib/premium-script/subdomain.conf
echo ${CF_ID} >> /var/lib/premium-script/cf_id.conf
echo ${CF_KEY} >> /var/lib/premium-script/cf_key.conf

#update certificate v2ray
certv2ray