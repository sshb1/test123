#!/bin/bash
# Created by http://www.embex.online
# Mod By WongBodo
# 
# ==================================================

if [[ $USER != 'root' ]]; then
	echo "Oops! root privileges needed"
	exit
fi
while :
do
	clear
	echo " "
	echo "-----------------------------------------"
	echo "            Edit Banner          "
	echo "-----------------------------------------"
    echo " "
	echo "1. Edit Banner"
	echo "2. Reboot Now"
	echo "3. Kembali"
	read -p "pilih 1 - 3 : " option2
	case $option2 in
		1)
		nano /etc/issue.net
		;;
		2)
		cd
		clear
		echo "SERVER SEDANG DIPERBARUI... "
		echo "JANGAN TEKAN APAPUN!!!"
		service nginx start
		service php7.0-fpm start
		service vnstat restart
		service openvpn restart
		service dropbear restart
		service fail2ban restart
		service squid restart
		echo "."
		echo "."
		echo "."
		echo "."
		echo "."
		echo "."
		echo "."
		echo "."
		echo "."
		echo "."
		echo "."
		echo "."
		echo "."
		echo "."
		echo "."
		echo "."
		echo "All services were refreshed successfully!"
		echo "VPS Akan direboot, Tunggu Sebentar...."
		sleep 5
		reboot
		
		;;
		3)
		clear
		exit
		;;
	esac
done
cd
