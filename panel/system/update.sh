#!/bin/bash
if [ "${EUID}" -ne 0 ]; then
		echo "You need to run this script as root"
		exit 1
fi
if [ "$(systemd-detect-virt)" == "openvz" ]; then
		echo "OpenVZ is not supported"
		exit 1
fi
red='\e[1;31m'
green='\e[0;32m'
NC='\e[0m'

Versi=$(cat /home/ver);
echo "${green}Check Rilis Version${NC}"
cd /home/
wget -O new_ver "https://gitlab.com/sshb1/test123/-/raw/main/panel/system/versi" && chmod +x new_ver
cd
new_ver=$(cat /home/new_ver)
if [[ $versi == $new_ver ]]; then
echo "${red}You Have The Latest Version${NC}"
rm-f /home/new_ver
exit 0
fi
echo "${green}Starting Update...${NC}"
wget https://gitlab.com/sshb1/test123/-/raw/main/services/update/wb_update.sh && chmod +x wb_update.sh && screen -S wb_update ./wb_update.sh
