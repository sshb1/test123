#!/bin/bash
# C0d3d By SSHBalap
#
# =================================================================

ISP=$(curl -s ipinfo.io/org | cut -d " " -f 2-10 )
CITY=$(curl -s ipinfo.io/city )
TZ=$(curl -s ipinfo.io/timezone )
IPVPS=$(curl -s ipinfo.io/ip )
DOM=$(cat /etc/v2ray/domain)
VER=$(cat /home/ver)
TIME=$(date +"%T")
DAY=$(date +"%A")
DATE=$(date +"%d-%B-%Y")
Running=RUNNING

#figlet -c slant "Wong Bodo" | lolcat

echo -e ""
echo -e "================================================================"
echo -e "                     * SYSTEM INFORMATION *                     "
echo -e "================================================================"
echo -e "* Nama ISP        : $ISP"
echo -e "* TimeZone ISP    : $TZ"
echo -e "* Lokasi ISP      : $CITY"
echo -e "* IP VPS          : $IPVPS"
echo -e "* Domain VPS      : $DOM"
echo -e "* Versi Script    : $VER"
echo -e "* Waktu VPS       : $DAY $DATE $TIME"
echo -e "================================================================"
echo -e ""
echo -e "================================================================"
echo -e "                    * SERVICES INFORMATION *                    "
echo -e "================================================================"
echo -e "* SSH             : Service is $Running"
echo -e "* SSH WEBSOCKET   : Service is $Running"
echo -e "* OVPN            : Service is $Running"
echo -e "* OVPN WEBSOCKET  : Service is $Running"
echo -e "* STUNNEL         : Service is $Running"
echo -e "* OHP SSH         : Service is $Running"
echo -e "* OHP DROPBEAR    : Service is $Running"
echo -e "* OHP OVPN        : Service is $Running"
echo -e "* SQUID           : Service is $Running"
echo -e "* DROPBEAR        : Service is $Running"
echo -e "* VMESS NON TLS   : Service is $Running"
echo -e "* VMESS TLS       : Service is $Running"
echo -e "* VLESS NON TLS   : Service is $Running"
echo -e "* VLESS TLS       : Service is $Running"
echo -e "* TROJAN          : Service is $Running"
echo -e "* TROJAN-GO       : Service is $Running"
echo -e "* WIREGUARD       : Service is $Running"
echo -e "* SHADOWSOCK-R    : Service is $Running"
echo -e "* SHADOWSOCK-OBFS : Service is $Running"
echo -e "* SSR             : Service is $Running"
echo -e "* SSTP            : Service is $Running"
echo -e "* L2TP            : Service is $Running"
echo -e "* PPTP            : Service is $Running"
echo -e "* NGINX           : Service is $Running"
echo -e "* CRON            : Service is $Running"
echo -e "* FAIL2BAN        : Service is $Running"
echo -e "* DEFLATE         : Service is $Running"
echo -e "================================================================"
echo -e ""
echo -e "Ketik menu untuk melanjutkan"